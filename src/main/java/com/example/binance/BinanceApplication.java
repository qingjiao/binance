package com.example.binance;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.binance.dao")
public class BinanceApplication {

    public static void main(String[] args) {
        System.setProperty("jdk.tls.useExtendedMasterSecret", "false");
        SpringApplication.run(BinanceApplication.class, args);
    }

}
