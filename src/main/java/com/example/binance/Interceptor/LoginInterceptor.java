package com.example.binance.Interceptor;

import com.example.binance.util.JWTUtil;
import io.jsonwebtoken.Claims;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.Iterator;

@Component
public class LoginInterceptor implements HandlerInterceptor {

    private static final String HEADER_AUTHORIZATION = "Authorization";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 从请求头中获取JWT令牌
        String token = request.getHeader(HEADER_AUTHORIZATION);

        Enumeration<String> headerNames = request.getHeaderNames();
        for (Iterator<String> it = headerNames.asIterator(); it.hasNext(); ) {
            String headerName = it.next();
            System.out.print(headerName + ":");
            for(Iterator<String> value = request.getHeaders(headerName).asIterator(); value.hasNext();){
                System.out.print(value.next());
                System.out.println(",");
            }
        }


        // 判断JWT令牌是否存在且以正确的前缀开头
        if (token != null ) {
            try {
                // 使用JWTUtil工具类解析JWT令牌
                Claims claims = JWTUtil.verifyToken(token);

                //判断校验
                String user = (String) claims.get("role");
                if (user.equals("admin")){
                    // 在请求中添加解析后的声明信息，方便后续处理
                    request.setAttribute("claims", claims);
                }

                return true; // 继续执行后续操作
            } catch (Exception e) {
                // JWT验证失败，返回错误信息给客户端
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().println("Invalid JWT token");
                return false;
            }
        } else {
            // 请求头缺少JWT令牌，返回错误信息给客户端
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().println("Missing JWT token");
            return false;
        }
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
