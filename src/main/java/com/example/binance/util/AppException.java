package com.example.binance.util;

public class AppException extends RuntimeException{

    private final int code;

    public AppException(int code, Throwable t) {
        super(t);
        this.code = code;
    }

    public AppException(int code) {
        this.code = code;
    }
}
