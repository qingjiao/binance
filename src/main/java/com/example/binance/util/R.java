/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.util;

import com.example.binance.constants.Code;

import java.io.Serial;

/**
 * The result of a method call or remote call
 */
public final class R<T> implements java.io.Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    public static final R<Void> SUCCESS = new R<>(Code.SUCCESS, null, null);
    private final int code;// Response status code
    private final T data; // Additional data
    private String message; // Additional Message

    /**
     * Create a failed result
     *
     * @param code Error code
     * @return failed result
     */
    public static <E> R<E> fail(int code) {
        return new R<>(code, null, null);
    }

    /**
     * Create a failed result
     *
     * @param code Error code
     * @param data Additional error data
     * @return failed result
     */
    public static <E> R<E> fail(int code, E data) {
        return new R<>(code, data, null);
    }

    /**
     * Create a failed result
     *
     * @param code    Error code
     * @param data    Additional error data
     * @param message Additional error message
     * @return failed result
     */
    public static <E> R<E> fail(int code, E data, String message) {
        return new R<>(code, data, message);
    }

    /**
     * Create a failed result
     *
     * @param code    Error code
     * @param message Additional error message
     * @return failed result
     */
    public static <E> R<E> fail(int code, String message) {
        return new R<>(code, null, message);
    }

    /**
     * Create a failed result
     *
     * @param code Error code
     * @param args Additional error message
     * @return failed result
     */
    public static <E> R<E> failWithArgs(int code, String... args) {
        return new R<>(code, null, String.join(",", args));
    }

    /**
     * convert from R<S>
     *
     * @param r source r
     * @return failed result
     */
    public static <E, S> R<E> fail(R<S> r) {
        return R.fail(r.code, null, r.getMessage());
    }

    /**
     * Create a successful result
     *
     * @param data Additional data
     * @return success result
     */
    public static <E> R<E> success(E data) {
        return new R<>(Code.SUCCESS, data, null);
    }

    @SuppressWarnings("unchecked")
    public static <E> R<E> to(R<Void> src) {
        return (R<E>) src;
    }

    /**
     * convert R<E> to R<Void>
     * @param src R<E>
     * @param <E> E
     * @return R<Void>
     */
    public static <E> R<Void> toVoid(R<E> src) {
        if (src.ok()) {
            return R.SUCCESS;
        }
        return new R<>(src.code, null, src.message);
    }

    private R(int code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    /**
     * Whether the object indicates a success state
     *
     * @return true: successful
     */
    public boolean ok() {
        return code == Code.SUCCESS;
    }

    /**
     * Whether the object indicates a success state
     *
     * @return true: failed
     */
    public boolean failed() {
        return code != Code.SUCCESS;
    }

    public int getCode() {
        return code;
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
