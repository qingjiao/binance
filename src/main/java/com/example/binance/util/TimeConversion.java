package com.example.binance.util;

public class TimeConversion {
    public static int convertToMinutes(String timeString) {
        int value = 0;
        String numericPart = timeString.substring(0, timeString.length() - 1);
        String unit = timeString.substring(timeString.length() - 1);

        try {
            int numericValue = Integer.parseInt(numericPart);

            switch (unit.toLowerCase()) {
                case "m":
                    value = numericValue;
                    break;
                case "h":
                    value = numericValue * 60;
                    break;
                case "d":
                    value = numericValue * 1440;
                    break;
                case "w":
                    value = numericValue * 10080;
                    break;
                case "M":
                    value = numericValue * 43200;
                    break;
                default:
                    throw new IllegalArgumentException("Invalid time unit: " + unit);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid time format: " + timeString);
        }

        return value;
    }
}
