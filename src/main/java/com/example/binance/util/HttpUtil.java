/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.util;

import static com.example.binance.core.GatewayConst.HEADER_VDX_IP;
import static com.example.binance.core.GatewayConst.HEADER_X_FORWARDED_FOR;
import static com.example.binance.core.GatewayConst.HEADER_X_REAL_IP;
import static com.example.binance.core.GatewayConst.UNKNOWN;

import com.example.binance.constants.Code;
import com.example.binance.core.GatewayConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Enumeration;

/**
 * http tool
 */
public class HttpUtil {

    private static final Logger LOG = LoggerFactory.getLogger(HttpUtil.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * to json string
     *
     * @param data data
     * @return string for json format
     */
    public static String toJsonString(Object data) {
        try {
            return mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            LOG.error("data convert to string error", e);
            throw new AppException(Code.SYSTEM_ERROR);
        }
    }

    /**
     * write body to response
     *
     * @param response    http response
     * @param httpCode    http response
     * @param contentType response content type
     * @param body        body
     */
    public static void writeError(HttpServletResponse response, int httpCode, String contentType, String body) {
        response.setContentType(contentType);
        response.setCharacterEncoding(Text.UTF8);
        response.setStatus(httpCode);
        try (Writer writer = response.getWriter()) {
            writer.write(body);
        } catch (IOException e) {
            LOG.warn("write response io exception", e);
        }
    }

    /**
     * get ip from header
     * <p>
     * HEADER_VDX_IP(pre put by dns or elb)
     *
     * @param request http request
     * @return ip
     */
    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader(HEADER_VDX_IP);
        if (StringUtils.hasText(ip) && !UNKNOWN.equals(ip)) {
            return ip;
        }

        request.getHeader(HEADER_X_FORWARDED_FOR);
        ip = getXffIp(request.getHeader(HEADER_X_FORWARDED_FOR));
        if (ip != null) {
            return ip;
        }

        ip = request.getHeader(HEADER_X_REAL_IP);
        if (StringUtils.hasText(ip) && !UNKNOWN.equals(ip)) {
            return ip;
        }
        return request.getRemoteAddr();
    }

    /**
     * x-forwarded-for is multiple split by ","
     *
     * @param ips ips, format: x.x.x.x,x.x.x.x
     * @return first ip
     */
    private static String getXffIp(String ips) {
        if (!StringUtils.hasText(ips) || ips.startsWith(UNKNOWN)) {
            return null;
        }

        int index = ips.indexOf(',');
        if (index == 0) {
            return null;
        }

        if (index == -1) {
            return ips;
        }
        return ips.substring(0, index).trim();
    }

    public static String getFingerprint(HttpServletRequest req) {
        String fingerprint = req.getHeader(GatewayConst.HEADER_FINGERPRINT);
        if (StringUtils.hasText(fingerprint)) {
            return fingerprint;
        }

        String ua = req.getHeader(HttpHeaders.USER_AGENT);
        if (StringUtils.hasText(ua)) {
            return EncryptUtil.sha256(ua);
        }
        StringBuilder sb = new StringBuilder();
        for (Enumeration<String> names = req.getHeaderNames(); names.hasMoreElements(); ) {
            sb.append(names.nextElement()).append(";");
        }
        LOG.warn("user agent is blank, path:{}, method:{}, headers:{}", req.getRequestURI(), req.getMethod(), sb);
        return Text.EMPTY;
    }
}
