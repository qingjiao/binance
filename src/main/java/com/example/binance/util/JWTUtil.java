package com.example.binance.util;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;
import java.security.Key;
import java.util.Date;

@Component
public class JWTUtil {

    public JWTUtil() {
    }

    private static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    private static final String HEADER_AUTHORIZATION = "Authorization";

    public static String generateToken(String userId,String subject, String role) {

        JwtBuilder jwtBuilder = Jwts.builder()
                .setId(userId)//唯一标识符声明
                .setSubject(subject)//主题声明
                .setIssuedAt(new Date())  //开始时间（签发时间）
                .setExpiration(new Date(System.currentTimeMillis() + 1800000))  //有效时间（过期时间）
                .signWith(SECRET_KEY,SignatureAlgorithm.HS512)
                .claim("role",role);

        // 设置其他的 JWT 配置

        return jwtBuilder.compact();
    }


//    public static void addTokenToRequestHeader(String token, HttpServletRequest request) {
//        if (token != null) {
//            request.setAttribute(HEADER_AUTHORIZATION, token);
//        }
//    }

    public static Claims verifyToken(String token) {
        Jws<Claims> jws = Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build()
                .parseClaimsJws(token);

        return jws.getBody();
    }
}
