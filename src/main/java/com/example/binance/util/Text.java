/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.util;

import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

public final class Text {

    public static final String EMPTY = "";
    public static final String PASSWORD_SPECIAL_CHARS = "!@-+_=.";
    public static final int MAX_SHOW_EMAIL_LENGTH = 3;
    public static final String MASK = "***";
    public static final String UTF8 = StandardCharsets.UTF_8.displayName();
    public static final Pattern NAME_FORMAT_PATTERN = Pattern.compile("^\\w+$");


    private Text() {
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", EMPTY);
    }

    public static boolean validateNameFormat(String name) {
        if (Objects.isNull(name)) {
            return false;
        }
        return NAME_FORMAT_PATTERN.matcher(name).matches();
    }

    /**
     * Keep the front and rear two char respectively
     * <p>
     * example: input : 123456 output: 12***56
     * <br>
     * input : 123 output: ***
     * <br>
     * input : 1234 output: 12***34
     *
     * @param v string
     * @return string
     */
    public static String hideString(String v) {
        if (Objects.isNull(v)) {
            return null;
        }
        v = v.trim();
        if (v.length() <= 3) {
            return MASK;
        }
        return v.substring(0, 2) + MASK + v.substring(v.length() - 2);
    }

    /**
     * Keep the front and rear two char respectively
     * <p>
     * example: input : abc@a.b output: ***@a.b
     *
     * @param email string
     * @return string
     */
    public static String hideEmail(String email) {
        if (Objects.isNull(email)) {
            return null;
        }
        email = email.trim();
        int index = email.indexOf('@');
        if (index < 0) {
            return email;
        }
        if (index >= MAX_SHOW_EMAIL_LENGTH) {
            return email.substring(0, MAX_SHOW_EMAIL_LENGTH) + MASK + email.substring(index);
        } else {
            return email.substring(0, index) + MASK + email.substring(index);
        }
    }

    public static int getPasswordLevel(String s) {
        int sum = 0;
        if (s.length() <= 8) {
            sum += 5;
        } else if (s.length() <= 15) {
            sum += 10;
        } else {
            sum += 25;
        }

        int countUpper = 0;
        int countLower = 0;
        int countNumber = 0;
        int countSpecial = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
                countUpper++;
            } else if (s.charAt(i) >= 'a' && s.charAt(i) <= 'z') {
                countLower++;
            } else if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
                countNumber++;
            } else if ((s.charAt(i) >= '!' && s.charAt(i) <= '/') ||
                    (s.charAt(i) >= ':' && s.charAt(i) <= '@') ||
                    (s.charAt(i) >= '[' && s.charAt(i) <= '`') ||
                    (s.charAt(i) >= '{' && s.charAt(i) <= '~')) {
                countSpecial++;
            } else {
                return -1;
            }
        }

        if (countNumber == 0 || countLower + countUpper == 0 || countSpecial == 0) {
            return -2;
        }

        if (countUpper > 1) {
            sum += 10;
        }
        if (countLower > 1) {
            sum += 10;
        }

        if (countNumber == 1) {
            sum += 10;
        } else if (countNumber > 1) {
            sum += 25;
        }

        if (countSpecial == 1) {
            sum += 10;
        } else if (countSpecial > 1) {
            sum += 25;
        }

        if (countUpper > 2 && countLower > 2 && countNumber > 2 && countSpecial > 2) {
            sum += 5;
        }
        return sum / 10;
    }
}
