/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.util;

import com.example.binance.constants.Code;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;


public class EncryptUtil {

    private static final String CODEC_SHA256 = "SHA-256";
    private static final String CODEC_AES = "AES";
    private static final String ALGORITHM_PBKDF2 = "PBKDF2WithHmacSHA512";
    private static final String TRANSFORMATION_AES = "AES/ECB/PKCS5Padding";
    private static final String TRANSFORMATION_AES_GCM = "AES/GCM/NoPadding";
    private static final int GCM_IV_LENGTH = 12;
    private static final int GCM_TAG_LENGTH = 16;
    private static final int PBKDF2_SALT_LENGTH = 16;
    private static final int PBKDF2_ITERATION_COUNT = 300000;
    private static final int PBKDF2_KEY_LENGTH = 512;

    /**
     * generate secret key specifies
     *
     * @param secret secret
     * @return specifies of secret key
     */
    public static SecretKeySpec secretKeyForAes(String secret) {
        return new SecretKeySpec(secret.getBytes(UTF_8), CODEC_AES);
    }

    /**
     * encrypt by aes
     *
     * @param secretKeySpec secret key specifies
     * @param source        source data
     * @return base64 code for encrypt data
     */
    public static String encryptAes(SecretKeySpec secretKeySpec, String source) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] encrypted = cipher.doFinal(source.getBytes(UTF_8));
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    /**
     * encrypt by aes
     *
     * @param secretKeySpec secret key specifies
     * @param source        source data
     * @return encrypt data
     */
    public static byte[] encryptAes(SecretKeySpec secretKeySpec, byte[] source) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            return cipher.doFinal(source);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    /**
     * decrypt by aes
     *
     * @param secretKeySpec secret key specifies
     * @param encrypted     base64 code for encrypted data
     * @return source data
     */
    public static String decryptAes(SecretKeySpec secretKeySpec, String encrypted) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] bytes = cipher.doFinal(Base64.getDecoder().decode(encrypted));
            return new String(bytes, UTF_8);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    /**
     * decrypt by aes
     *
     * @param secretKeySpec secret key specifies
     * @param encrypted     encrypted data
     * @return source data
     */
    public static byte[] decryptAes(SecretKeySpec secretKeySpec, byte[] encrypted) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            return cipher.doFinal(encrypted);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    public static byte[] encryptAesGcm(SecretKeySpec secretKeySpec, String source) {
        try {
            byte[] bytes = source.getBytes(UTF_8);

            // iv
            byte[] iv = new byte[GCM_IV_LENGTH];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);

            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, iv);
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES_GCM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);
            byte[] encryptedData = cipher.doFinal(bytes);
            byte[] r = new byte[iv.length + encryptedData.length];
            System.arraycopy(iv, 0, r, 0, iv.length);
            System.arraycopy(encryptedData, 0, r, iv.length, encryptedData.length);
            return r;
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    public static byte[] decryptAesGcm(SecretKeySpec secretKeySpec, byte[] data) {
        try {
            Cipher cipher = Cipher.getInstance(TRANSFORMATION_AES_GCM);
            GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, data, 0, GCM_IV_LENGTH);
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);
            return cipher.doFinal(data, GCM_IV_LENGTH, data.length - GCM_IV_LENGTH);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    /**
     * hash by sha256
     *
     * @param source source data
     * @return base64 code for hash data
     */
    public static String sha256(String source) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(CODEC_SHA256);
            messageDigest.update(source.getBytes(UTF_8));
            return Base64.getEncoder().encodeToString(messageDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    public static String pbkdf2(String password) {
        try {
            SecureRandom random = new SecureRandom();
            byte[] salt = new byte[PBKDF2_SALT_LENGTH];
            random.nextBytes(salt);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM_PBKDF2);
            KeySpec passwordBasedEncryptionKeySpec = new PBEKeySpec(password.toCharArray(), salt, PBKDF2_ITERATION_COUNT, PBKDF2_KEY_LENGTH);
            SecretKey secretKeyFromPBKDF2 = secretKeyFactory.generateSecret(passwordBasedEncryptionKeySpec);
            byte[] pwd = secretKeyFromPBKDF2.getEncoded();
            byte[] r = new byte[salt.length + pwd.length];
            System.arraycopy(salt, 0, r, 0, salt.length);
            System.arraycopy(pwd, 0, r, salt.length, pwd.length);
            return Base64.getEncoder().encodeToString(r);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }

    public static boolean verifyPbkdf2(String password, String hash) {
        try {
            byte[] hashBytes = Base64.getDecoder().decode(hash);
            byte[] salt = new byte[PBKDF2_SALT_LENGTH];
            System.arraycopy(hashBytes, 0, salt, 0, salt.length);

            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM_PBKDF2);
            KeySpec passwordBasedEncryptionKeySpec = new PBEKeySpec(password.toCharArray(), salt, PBKDF2_ITERATION_COUNT, PBKDF2_KEY_LENGTH);
            SecretKey secretKeyFromPBKDF2 = secretKeyFactory.generateSecret(passwordBasedEncryptionKeySpec);
            byte[] pwd = secretKeyFromPBKDF2.getEncoded();
            return Arrays.equals(hashBytes, salt.length, hashBytes.length - 1, pwd, 0, pwd.length - 1);
        } catch (Exception e) {
            throw new AppException(Code.SYSTEM_ERROR, e);
        }
    }
}
