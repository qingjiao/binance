package com.example.binance.entity;

import java.math.BigDecimal;

public class KlineData {

    private Long id;

    private String symbol;

    private Integer timeFrame;//

    private Long time;

    private BigDecimal open;

    private BigDecimal high;

    private BigDecimal low;

    private BigDecimal close;

    private BigDecimal volume;

    private Long closeTime;//

    private BigDecimal amount;

    private Integer tradesCount;//

    private BigDecimal takerBuyVolume;//

    private BigDecimal takerBuyAmount;//


    public Long getId() {
        return id;
    }

    public KlineData setId(Long id) {
        this.id = id;
        return this;
    }

    public String getSymbol() {
        return symbol;
    }

    public KlineData setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public Integer getTimeFrame() {
        return timeFrame;
    }

    public KlineData setTimeFrame(Integer timeFrame) {
        this.timeFrame = timeFrame;
        return this;
    }

    public Long getTime() {
        return time;
    }

    public KlineData setTime(Long time) {
        this.time = time;
        return this;
    }

    public BigDecimal getOpen() {
        return open;
    }

    public KlineData setOpen(BigDecimal open) {
        this.open = open;
        return this;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public KlineData setHigh(BigDecimal high) {
        this.high = high;
        return this;
    }

    public BigDecimal getLow() {
        return low;
    }

    public KlineData setLow(BigDecimal low) {
        this.low = low;
        return this;
    }

    public BigDecimal getClose() {
        return close;
    }

    public KlineData setClose(BigDecimal close) {
        this.close = close;
        return this;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public KlineData setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }

    public Long getCloseTime() {
        return closeTime;
    }

    public KlineData setCloseTime(Long closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public KlineData setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Integer getTradesCount() {
        return tradesCount;
    }

    public KlineData setTradesCount(Integer tradesCount) {
        this.tradesCount = tradesCount;
        return this;
    }

    public BigDecimal getTakerBuyVolume() {
        return takerBuyVolume;
    }

    public KlineData setTakerBuyVolume(BigDecimal takerBuyVolume) {
        this.takerBuyVolume = takerBuyVolume;
        return this;
    }

    public BigDecimal getTakerBuyAmount() {
        return takerBuyAmount;
    }

    public KlineData setTakerBuyAmount(BigDecimal takerBuyAmount) {
        this.takerBuyAmount = takerBuyAmount;
        return this;
    }

    @Override
    public String toString() {
        return "KlineData{" +
                "id=" + id +
                ", symbol='" + symbol + '\'' +
                ", timeFrame='" + timeFrame + '\'' +
                ", time=" + time +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                ", volume=" + volume +
                ", closeTime=" + closeTime +
                ", amount=" + amount +
                ", tradesCount=" + tradesCount +
                ", takerBuyVolume=" + takerBuyVolume +
                ", takerBuyAmount=" + takerBuyAmount +
                '}';
    }
}
