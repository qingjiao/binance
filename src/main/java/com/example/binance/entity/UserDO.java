package com.example.binance.entity;

public class UserDO {
    private Integer id;
    private String username;
    private String password;
    private String email;
    private Integer userState;
    private String role;
    private Long createTime;
    private Long updateTime;


    public Integer getId() {
        return id;
    }

    public UserDO setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserDO setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getUserState() {
        return userState;
    }

    public UserDO setUserState(Integer userState) {
        this.userState = userState;
        return this;
    }

    public String getRole() {
        return role;
    }

    public UserDO setRole(String role) {
        this.role = role;
        return this;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public UserDO setCreateTime(Long createTime) {
        this.createTime = createTime;
        return this;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public UserDO setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "UserDO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", userState=" + userState +
                ", role='" + role + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
