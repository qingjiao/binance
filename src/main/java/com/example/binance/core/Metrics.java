/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.core;

import java.lang.annotation.*;

/**
 * auto implementation metric
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Metrics {

    String name() default "";

    String template() default "";

    String help() default "";

    String[] labels() default {};

    // histogram
    double[] buckets() default {};

    // summary
    long maxAgeSeconds() default 600;

    // summary
    int ageBuckets() default 5;

    // summary quantileValues.length == quantileErrors.length
    double[] quantileValues() default {};

    // summary quantileValues.length == quantileErrors.length
    double[] quantileErrors() default {};

}
