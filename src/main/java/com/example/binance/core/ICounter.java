package com.example.binance.core;

public interface ICounter extends IMetrics {

    void counter(double value, String... labelValues);
}
