/*
 * Copyright 2021 Victory Fintech Co. Ltd.
 */
package com.example.binance.core;

import com.example.binance.constants.Code;
import com.example.binance.util.HttpUtil;
import com.example.binance.util.R;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

public interface GatewayConst {

    String UNKNOWN = "unknown";

    String HEADER_VDX_IP = "VDX-IP";
    String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
    String HEADER_X_REAL_IP = "X-Real-IP";
    String HEADER_FINGERPRINT = "VDX-FINGERPRINT";
    String HEADER_TOKEN = "VDX-TOKEN";

    /**
     * the response content for forbidden error
     */
    String RESPONSE_FORBIDDEN_ERROR = HttpUtil.toJsonString(R.fail(Code.FORBIDDEN, "forbid access."));
    /**
     * the response content for forbidden error
     */
    String RESPONSE_TOO_MANY_REQUESTS_ERROR = HttpUtil.toJsonString(R.fail(Code.TOO_MANY_REQUESTS, "too many requests."));
    /**
     * the response content for system error
     */
    String RESPONSE_SYSTEM_ERROR = HttpUtil.toJsonString(R.fail(Code.SYSTEM_ERROR, "system error."));
    /**
     * the response content for illegal parameter error
     */
    String RESPONSE_ILLEGAL_PARAMETER = HttpUtil.toJsonString(R.fail(Code.ILLEGAL_PARAMETER, "illegal parameter."));
    /**
     * the response content for missing parameter error
     */
    String RESPONSE_MISSING_PARAMETER = HttpUtil.toJsonString(R.fail(Code.MISSING_PARAMETER, "missing parameter."));
    /**
     * the response content for missing parameter error
     */
    String RESPONSE_NOT_FOUND = HttpUtil.toJsonString(R.fail(Code.NOT_FOUNT, "not found."));

    /**
     * rest response content type
     */
    String CONTENT_TYPE_JSON = "application/json;charset=utf-8";

    /**
     * IP field name in jwt
     */
    String JWT_PAYLOAD_IP_FIELD = "ip";

    /**
     * The country code for Hong Kong
     */
    int COUNTRY_CODE_HONG_KONG = 344;
    /**
     * The default fingerprint, use it as fingerprint, because there is no fingerprint integration now.
     */
    String DEFAULT_FINGERPRINT = "-";

    /**
     * default query size
     */
    int DEFAULT_QUERY_SIZE = 10;
    /**
     * max query size
     */
    int MAX_QUERY_SIZE = 100;
    /**
     * default query size
     */
    long ZERO_LONG = 0;
    int ZERO_INT = 0;
    /**
     * default scale
     */
    int DEFAULT_SCALE = 2;
    /**
     * for percent multiple
     */
    BigDecimal PERCENT_MULTIPLE = BigDecimal.valueOf(100);

    int ASSET_DEFAULT_SCALE = 8;

    String OMS_SHARD_CONFIG = "oms";

    int TRUE = 1;
    int FALSE = 0;

    String FLOW_LIMIT_SUFFIX = "@limit@bgw";
    String FLOW_LIMIT_USER_SUFFIX = "@u@limit@bgw";
    String FLOW_LIMIT_IP_SUFFIX = "@ip@limit@bgw";
    long TIME_SCALE_DAY_MILLIS = TimeUnit.DAYS.toMillis(1);
    long TIME_SCALE_DAY_30_MILLIS = TimeUnit.DAYS.toMillis(30);

    /**
     * api key expired time, unit is milliseconds
     */
    long API_KEY_MAX_EXPIRED_TIME = TimeUnit.DAYS.toMillis(90);
    /**
     * bind max account size for one api key
     */
    int MAX_API_KEY_ACCOUNT_SIZE = 5;
}
