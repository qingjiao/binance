package com.example.binance.constants;

public enum UserStatusEnum {
    ACTIVE(1),
    DISABLED(2);

    public final int STATUS;

    UserStatusEnum(int status) {
        STATUS = status;
    }
}
