package com.example.binance.constants;

public interface Code {

    //非法字符
    int ILLEGAL_PARAMETER = 9901;

    //成功
    int SUCCESS = 9200;

    //账号密码错误
    int ACCOUNT_PASSWORD_INCORRECT = 6100;

    //系统错误
    int SYSTEM_ERROR = 9500;

    //禁止
    int FORBIDDEN = 9403;

    //未找到
    int NOT_FOUNT = 9404;

    //请求过多
    int TOO_MANY_REQUESTS = 9429;

    //无权限
    int NO_PERMISSION = 9450;

    //缺失参数
    int MISSING_PARAMETER = 9900;

    //超过最大上传大小
    int MAX_UPLOAD_SIZE_EXCEEDED = 9902;

    //不支持的文件类型
    int UNSUPPORTED_FILE_TYPE = 9903;

    //用户已存在
    int USER_EXISTS = 6200;

}
