package com.example.binance.controller;

import com.example.binance.constants.Code;
import com.example.binance.entity.UserDO;
import com.example.binance.model.CreateUserBO;
import com.example.binance.model.UserLoginBO;
import com.example.binance.service.UserService;
import com.example.binance.util.JWTUtil;
import com.example.binance.util.R;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Validated
@RestController
@RequestMapping("/klines")
@CrossOrigin(origins = "*")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/create")
    public R<Void> creatUser(CreateUserBO createUserBO){
        return userService.creatUser(createUserBO);
    }


    @PostMapping("/login")
    public R<String> login(@RequestBody UserLoginBO userLoginBO){
        return userService.login(userLoginBO);
    }


}
