package com.example.binance.controller;

import com.example.binance.entity.KlineData;
import com.example.binance.model.ListSymbolBO;
import com.example.binance.model.UpdateKlineBO;
import com.example.binance.service.KlineDataService;
import com.example.binance.util.R;
import com.example.binance.util.RedisUtil;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;


@Validated
@RestController
@RequestMapping("/klines")
public class KlineController {

    @Autowired
    private KlineDataService klineDataService;

    @Autowired
    private RedisUtil redisUtil;


    /**
     * 根据binance接口获取K线数据，存储数据库
     * @param updateKlineBO
     * @return
     */
    @GetMapping("/list")
    public R<Void> listKlineData(UpdateKlineBO updateKlineBO) {
        return klineDataService.listKlineData(updateKlineBO);
    }


    /**
     * 以K线间隔查询数据
     */
    @GetMapping("/time")
    public R<List<KlineData>> list(ListSymbolBO listSymbolBO) {
        listSymbolBO.check();
        String key = "Kline" + listSymbolBO.getSymbol() + listSymbolBO.getTimeFrame();

        List<KlineData> klineDataList = (List<KlineData>) redisUtil.get(key);
        if (klineDataList == null) {
            R<List<KlineData>> listR = klineDataService.list(listSymbolBO);
            if (listR.failed()) {
                return listR;
            }
            klineDataList = listR.getData();
            redisUtil.set(key, klineDataList);
        }
        return R.success(klineDataList);
    }


    /**
     * 更新K线数据
     *
     * @param updateKlineBO
     * @return
     */
    @GetMapping("/renewal")
    public R<Void> renewalKlineData(UpdateKlineBO updateKlineBO) {
        return klineDataService.renewalKlineData(updateKlineBO);
    }


    /**
     * 查询打印压缩K线数据到本地，并返回文件路径
     *
     * @param symbol
     * @param interval
     * @return
     */
    @GetMapping("/print")
    public R<String> printKlineData(@NotNull String symbol, @NotNull String interval) {
        return klineDataService.printKlineData(symbol, interval);
    }


    /**
     * 根据文件路径解压数据存储到数据库中
     * @param filePath
     * @return
     * @throws IOException
     */
    @PostMapping("/unzip")
    public R<Void> unZipKlineData(@NotNull String filePath) throws IOException {
        return klineDataService.unZipKlineData(filePath);
    }
}
