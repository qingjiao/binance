package com.example.binance.service;

import com.example.binance.util.EncryptUtil;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.spec.SecretKeySpec;


@Service
public class EncryptService {

    private static final String aesSecretKey = "BinanceManageFixedAESKey";

    private SecretKeySpec aesSecretKeySpec;

    @PostConstruct
    public void initAesSecretKeySpec() {
        this.aesSecretKeySpec = EncryptUtil.secretKeyForAes(aesSecretKey);
    }

    /**
     *  加密邮箱地址
     * @param email 明文邮箱地址
     * @return 加密后邮箱地址
     */
    public String encryptEmail(String email) {
        return aesEncrypt(email);
    }


    /**
     * 解密邮箱地址
     * @param encryptedEmail 密文邮箱地址
     * @return 解密后邮箱地址
     */
    public String decryptEmail(String encryptedEmail) {
        return aesDecrypt(encryptedEmail);
    }


    /**
     * 原始密码进行加密
     * @param password 密码明文
     * @return
     */
    public String encryptPassword(String password) {
        return aesEncrypt(password);
    }


    /**
     * 加密后的密码进行解密
     * @param encryptedPassword 密文密码
     * @return
     */
    public String decryptPassword(String encryptedPassword) {
        return aesDecrypt(encryptedPassword);
    }


    private String aesEncrypt(String originalString) {
        return EncryptUtil.encryptAes(this.aesSecretKeySpec, originalString);
    }

    private String aesDecrypt(String encryptedString) {
        return EncryptUtil.decryptAes(this.aesSecretKeySpec, encryptedString);
    }
}
