package com.example.binance.service;

import com.example.binance.constants.Constants;
import com.example.binance.dao.KlineDataDao;
import com.example.binance.entity.KlineData;
import com.example.binance.model.ListSymbolBO;
import com.example.binance.model.UpdateKlineBO;
import com.example.binance.util.JSON;
import com.example.binance.util.R;
import com.example.binance.util.TimeConversion;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import java.io.*;
import java.math.BigDecimal;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Aspect
@Service
public class KlineDataService {

    private static final Logger LOG = LoggerFactory.getLogger(KlineDataService.class);

    @Resource
    private KlineDataDao klineDataDao;

    public R<Void> listKlineData(UpdateKlineBO updateKlineBO) {
        System.setProperty("jdk.tls.useExtendedMasterSecret", "false");
        Security.setProperty("jdk.certpath.disabledAlgorithms", "");
        System.setProperty("javax.net.debug", "ssl");
        System.setProperty("jdk.internal.httpclient.disableHostnameVerification", Boolean.TRUE.toString());

        HttpClient httpClient = HttpClientBuilder.create().build();
        String url;
        if (updateKlineBO.getStartTime() == null){
            url = "https://api.binance.com/api/v3/klines?symbol=" + updateKlineBO.getSymbol() + "&interval=" + updateKlineBO.getInterval() + "&limit=" + updateKlineBO.getLimit();
        }else {
            url = "https://api.binance.com/api/v3/klines?symbol=" + updateKlineBO.getSymbol() + "&interval=" + updateKlineBO.getInterval() + "&startTime=" + updateKlineBO.getStartTime() +"&limit=" + updateKlineBO.getLimit();
        }
        HttpGet request = new HttpGet(url);
        request.setConfig(RequestConfig.custom().setConnectTimeout(30000).setConnectionRequestTimeout(30000).build());

        try {
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == 200) {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();

                // 解析JSON并保存K线数据到数据库
                String jsonString = sb.toString();
                TypeReference<List<List<Object>>> tr = new TypeReference<>() {
                };
                List<List<Object>> lists = JSON.parse(jsonString, tr);
                List<KlineData> klineDataList = new ArrayList<>();
                int time = TimeConversion.convertToMinutes(updateKlineBO.getInterval());

                int count = 0;

                for (List<Object> l : lists) {
                    KlineData klineData1 = new KlineData()
                            .setSymbol(updateKlineBO.getSymbol())
                            .setTimeFrame(time)
                            .setTime((Long) l.get(0))
                            .setOpen(new BigDecimal((String) l.get(1)))
                            .setHigh(new BigDecimal((String) l.get(2)))
                            .setLow(new BigDecimal((String) l.get(3)))
                            .setClose(new BigDecimal((String) l.get(4)))
                            .setVolume(new BigDecimal((String) l.get(5)))
                            .setCloseTime((Long) l.get(6))
                            .setAmount(new BigDecimal((String) l.get(7)))
                            .setTradesCount((Integer) l.get(8))
                            .setTakerBuyVolume(new BigDecimal((String) l.get(9)))
                            .setTakerBuyAmount(new BigDecimal((String) l.get(10)));

                    klineDataList.add(klineData1);
                    count++;

                    if (count == 100) {
                        klineDataDao.insertKlineData(klineDataList);
                        klineDataList.clear();
                        count = 0;
                    }
                }
                if (!klineDataList.isEmpty()) {
                    klineDataDao.insertKlineData(klineDataList);
                }

                LOG.info("请求地址：{}", ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.SUCCESS;
    }

    @Transactional
    public R<List<KlineData>> list(ListSymbolBO listSymbolBO) {

        List<KlineData> list = klineDataDao.list(listSymbolBO);
        LOG.info("请求地址：{}", ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());

        return R.success(list);
    }


    public R<Void> renewalKlineData(UpdateKlineBO updateKlineBO){
        int time = TimeConversion.convertToMinutes(updateKlineBO.getInterval());
        KlineData maxTime = klineDataDao.getMaxTime(updateKlineBO.getSymbol(), time);
        //如果最大时间数据为null，则没有该币种或时间间隔的数据
        if (maxTime == null){
            updateKlineBO.setStartTime(Constants.ZERO);
            listKlineData(updateKlineBO);
        }else {
            updateKlineBO.setStartTime(maxTime.getTime());
            listKlineData(updateKlineBO);
        }
        return R.SUCCESS;
    }



    public R<String> printKlineData(String symbol, String interval) {
        int time = TimeConversion.convertToMinutes(interval);
        List<KlineData> klineDataList = klineDataDao.listAllKline(symbol, time);

        String zipFilePath = "src/main/resources/data/a.zip";
        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFilePath));
             DataOutputStream dos = new DataOutputStream(new FileOutputStream("src/main/resources/data/a.txt"))) {

            for (KlineData klineData : klineDataList) {
                dos.writeLong(klineData.getId());
                dos.writeUTF(klineData.getSymbol());
                dos.writeInt(klineData.getTimeFrame());
                dos.writeLong(klineData.getTime());
                dos.writeDouble(klineData.getOpen().doubleValue());
                dos.writeDouble(klineData.getHigh().doubleValue());
                dos.writeDouble(klineData.getLow().doubleValue());
                dos.writeDouble(klineData.getClose().doubleValue());
                dos.writeDouble(klineData.getVolume().doubleValue());
                dos.writeLong(klineData.getCloseTime());
                dos.writeDouble(klineData.getAmount().doubleValue());
                dos.writeInt(klineData.getTradesCount());
                dos.writeDouble(klineData.getTakerBuyVolume().doubleValue());
                dos.writeDouble(klineData.getTakerBuyAmount().doubleValue());
            }

            File fileToZip = new File("src/main/resources/data/a.txt");
            zipFile(fileToZip, fileToZip.getName(), zipOut);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return R.success(zipFilePath);
    }


    private void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }

        fis.close();
        zipOut.closeEntry();
        zipOut.close();
    }


    public R<Void> unZipKlineData(String filePath) throws IOException {
            File zipFile = new File(filePath);

            try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFile))) {
                ZipEntry entry = zipIn.getNextEntry();

                while (entry != null) {
                    if (!entry.isDirectory()) {
//                        String file = targetDirectory + File.separator + entry.getName();
                        byte[] fileContent = extractFile(zipIn);
                        convertAndStoreData(fileContent);
                    }
                    entry = zipIn.getNextEntry();
                }
                zipIn.closeEntry();
            }
            return R.SUCCESS;
        }


        private byte[] extractFile(ZipInputStream zipIn) throws IOException {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            try {
                byte[] bytesIn = new byte[1024];
                int read;

                while ((read = zipIn.read(bytesIn)) != -1) {
                    bos.write(bytesIn, 0, read);
                }

                return bos.toByteArray();
            } finally {
                bos.close();
            }
        }

    private void convertAndStoreData(byte[] content) {
        try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(content))) {
            // 从二进制数据中读取字段并进行相应的转换
            // 将字段存储到数据库中
            while (dis.available() > 0) {
                KlineData klineData = new KlineData()
                        .setId(dis.readLong())
                        .setSymbol(dis.readUTF())
                        .setTimeFrame(dis.readInt())
                        .setTime(dis.readLong())
                        .setOpen(BigDecimal.valueOf(dis.readDouble()))
                        .setHigh(BigDecimal.valueOf(dis.readDouble()))
                        .setLow(BigDecimal.valueOf(dis.readDouble()))
                        .setClose(BigDecimal.valueOf(dis.readDouble()))
                        .setVolume(BigDecimal.valueOf(dis.readDouble()))
                        .setCloseTime(dis.readLong())
                        .setAmount(BigDecimal.valueOf(dis.readDouble()))
                        .setTradesCount(dis.readInt())
                        .setTakerBuyVolume(BigDecimal.valueOf(dis.readDouble()))
                        .setTakerBuyAmount(BigDecimal.valueOf(dis.readDouble()));
                // 将字段存储到数据库中
                klineDataDao.saveKline(klineData);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
