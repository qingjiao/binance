package com.example.binance.service;

import com.example.binance.constants.Code;
import com.example.binance.constants.UserStatusEnum;
import com.example.binance.dao.UserDao;
import com.example.binance.entity.UserDO;
import com.example.binance.model.CreateUserBO;
import com.example.binance.model.UserLoginBO;
import com.example.binance.util.JWTUtil;
import com.example.binance.util.R;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserService {

    @Resource
    private UserDao userDao;

    @Resource
    private EncryptService encryptService;


    public R<Void> creatUser(CreateUserBO createUserBO){

        UserDO userByUsername = userDao.getUserByUsername(createUserBO.getUsername());
        if (userByUsername != null){
            return R.fail(Code.USER_EXISTS,null,"用户名已存在！");
        }

        UserDO userDO = new UserDO()
                .setUsername(createUserBO.getUsername())
                .setPassword(encryptService.encryptPassword(createUserBO.getPassword()))
                .setEmail(createUserBO.getEmail())
                .setUserState(UserStatusEnum.ACTIVE.STATUS)
                .setRole(createUserBO.getRole())
                .setCreateTime(System.currentTimeMillis())
                .setUpdateTime(System.currentTimeMillis());

        userDao.creatUser(userDO);
        return R.SUCCESS;
    }

    public R<String> login(UserLoginBO userLoginBO){
        //密码加密
        userLoginBO.setPassword(encryptService.encryptPassword(userLoginBO.getPassword()));
        //查询账号密码是否正确
        UserDO userByUsername = userDao.getUserByUsernamePassword(userLoginBO);
        if (userByUsername == null){
            return R.fail(Code.ACCOUNT_PASSWORD_INCORRECT,null,"账号密码不正确！");
        }
        String token = JWTUtil.generateToken(userByUsername.getId().toString(), "JWT",userByUsername.getRole());

        return R.success(token);
    }

}
