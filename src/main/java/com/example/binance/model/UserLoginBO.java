package com.example.binance.model;

public class UserLoginBO {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public UserLoginBO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserLoginBO setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "UserLoginBO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
