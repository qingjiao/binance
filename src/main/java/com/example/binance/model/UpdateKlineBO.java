package com.example.binance.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class UpdateKlineBO {

    @NotNull
    private String symbol;

    @NotNull
    private String interval;

    @Max(1000)
    @Min(1)
    private Integer limit;

    private Long startTime;

    public String getSymbol() {
        return symbol;
    }

    public UpdateKlineBO setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public String getInterval() {
        return interval;
    }

    public UpdateKlineBO setInterval(String interval) {
        this.interval = interval;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public UpdateKlineBO setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public Long getStartTime() {
        return startTime;
    }

    public UpdateKlineBO setStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    @Override
    public String toString() {
        return "UpdateKlineBO{" +
                "symbol='" + symbol + '\'' +
                ", interval='" + interval + '\'' +
                ", limit=" + limit +
                ", startTime=" + startTime +
                '}';
    }
}
