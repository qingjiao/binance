package com.example.binance.model;

public class CreateUserBO {

    private String username;
    private String password;
    private String email;
    private String role;

    public String getUsername() {
        return username;
    }

    public CreateUserBO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CreateUserBO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public CreateUserBO setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getRole() {
        return role;
    }

    public CreateUserBO setRole(String role) {
        this.role = role;
        return this;
    }

    @Override
    public String toString() {
        return "CreateUserBO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                '}';
    }
}
