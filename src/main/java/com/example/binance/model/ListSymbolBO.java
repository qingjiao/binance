package com.example.binance.model;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import org.springframework.lang.NonNull;


public class ListSymbolBO {

    @NonNull
    private String Symbol;

    @NonNull
    private Integer timeFrame;

    @Min(0)
    private Long before;

    @Min(0)
    private Long after;

    @Min(0)
    @Max(20)
    private Integer limit;

    public ListSymbolBO getListSymbolBO() {
        return this;
    }


    public void check() {
        // 判断before,after,limit是否为空,为空赋默认值
        if (limit == null) {
            limit = 10;
        }
        if (before == null) {
            before = 0L;
        }
        if (after == null) {
            after = 0L;
        }
        // 判断before,after是否同时存在,存在则before为0
        before = after > 0 ? 0 : before;
    }


    @NonNull
    public String getSymbol() {
        return Symbol;
    }

    public ListSymbolBO setSymbol(@NonNull String symbol) {
        Symbol = symbol;
        return this;
    }

    @NonNull
    public Integer getTimeFrame() {
        return timeFrame;
    }

    public ListSymbolBO setTimeFrame(@NonNull Integer timeFrame) {
        this.timeFrame = timeFrame;
        return this;
    }

    public Long getBefore() {
        return before;
    }

    public ListSymbolBO setBefore(Long before) {
        this.before = before;
        return this;
    }

    public Long getAfter() {
        return after;
    }

    public ListSymbolBO setAfter(Long after) {
        this.after = after;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public ListSymbolBO setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }


    @Override
    public String toString() {
        return "ListSymbolBO{" +
                "Symbol='" + Symbol + '\'' +
                ", timeFrame=" + timeFrame +
                ", before=" + before +
                ", after=" + after +
                ", limit=" + limit +
                '}';
    }
}
