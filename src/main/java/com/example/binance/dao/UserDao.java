package com.example.binance.dao;

import com.example.binance.entity.UserDO;
import com.example.binance.model.UserLoginBO;
import org.apache.ibatis.annotations.Param;

public interface UserDao {

    void creatUser(UserDO userDO);

    UserDO getUserByUsernamePassword(UserLoginBO userLoginBO);

    UserDO getUserByUsername(@Param("username") String username);
}
