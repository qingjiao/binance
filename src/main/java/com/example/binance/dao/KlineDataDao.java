package com.example.binance.dao;


import com.example.binance.entity.KlineData;
import com.example.binance.model.ListSymbolBO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface KlineDataDao {

    void insertKlineData(List<KlineData> klineDataList);


    List<KlineData> list(ListSymbolBO listSymbolBO);


    KlineData getMaxTime(@Param("symbol") String symbol, @Param("time")Integer time);

    List<KlineData> listAllKline(@Param("symbol") String symbol, @Param("time")Integer time);

    void saveKline(@Param("klineData") KlineData klineData);
}
